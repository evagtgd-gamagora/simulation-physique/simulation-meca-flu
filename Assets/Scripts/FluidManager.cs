﻿//#define STRUCT
#define HASH
//#define TEST

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class FluidManager : MonoBehaviour {

    public const int n_particles    = 800;
    private const float minX_pix    = 420;//= 20;
    private const float maxX_pix    = 600;//= 1000;
    private const float minY_pix    = 0;
    private const float maxY_pix    = 700;

    private const float pixelPerMeter = 20;

    private const float minX = minX_pix / pixelPerMeter;
    private const float maxX = maxX_pix / pixelPerMeter;
    private const float minY = -maxY_pix / pixelPerMeter; 
    private const float maxY = -minY_pix / pixelPerMeter;

    
    private Vector2 gravity         = Vector2.down * 9.807f;
    private const float mass        = 0.01f;
    private const float alpha       = 5f;

#if STRUCT
    public const float h            = 1f;
#else
    public float h                  = 1f;
#endif

    public float k          = 5f;
    public float k_near     = 5f;
    public float rho_0      = 0.1f;
    public float sigma      = 1f;
    public float beta       = 1f;

    private float deltaT;
    private float deltaT2;

    public Texture texture;

    private Vector2[] particules_position;
    private Vector2[] particules_previous_position;
    private Vector2[] particules_velocity;

#if STRUCT || HASH
    private float neighbors_struct_size;
    private const int offset = 10;
    private int neighbors_struct_nX;
    private int neighbors_struct_nY;
#endif

#if STRUCT
    private int[][] neighbors_struct;
#endif

#if HASH
    private Dictionary<Vector2Int, List<int>> hash_grid;
#endif

    // Use this for initialization
    void Start () {
        particules_position             = new Vector2[n_particles];
        particules_previous_position    = new Vector2[n_particles];
        particules_velocity             = new Vector2[n_particles];

#if STRUCT || HASH
    neighbors_struct_size = h;
    neighbors_struct_nX = Mathf.FloorToInt((maxX - minX) / neighbors_struct_size) + 2 * offset;
    neighbors_struct_nY = Mathf.FloorToInt((maxY - minY) / neighbors_struct_size) + 2 * offset;
#endif

#if STRUCT
        neighbors_struct = new int[neighbors_struct_nX * neighbors_struct_nY][];
        for (int i = 0; i < neighbors_struct_nX * neighbors_struct_nY; ++i)
        {
            neighbors_struct[i] = new int[n_particles + 1];
            neighbors_struct[i][0] = 0;
        }
#endif

        for (int i = 0; i < n_particles; i++)
        {
            particules_position[i] = new Vector2(Random.Range(minX, maxX), Random.Range(minY + (maxY-minY)/2, maxY));
            particules_velocity[i] = new Vector2(Random.Range(-10, 10), Random.Range(-10, 10));
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        deltaT = Time.deltaTime;
        deltaT2 = deltaT * deltaT;

        ApplyGravity();

        //ApplyViscosity();

        SavePreviousPosition();
        ApplyVelocity();

        ApplySpringDeplacement();
        DoubleDensityRelaxation();

        RecalculateVelocity();
    }

    void OnGUI()
    {
        if (Event.current.type == EventType.Repaint)
        {
            for (int i = 0; i < n_particles; i++)
            {
                Rect rect = new Rect(particules_position[i].x * pixelPerMeter, - particules_position[i].y * pixelPerMeter, 10, 10);
                Graphics.DrawTexture(rect, texture);
            }
        }
    }


    private void ApplyGravity()
    {
        for (int i = 0; i < n_particles; i++)
        {
            particules_velocity[i] += gravity * deltaT;
        }
    }

    private void ApplyViscosity()
    {

#if STRUCT || HASH
        CalculateNeighborsStruct();
#endif

        for (int particule = 0; particule < n_particles; particule++)
        {

#if STRUCT || HASH
            List<particule_distance> neighbors = GetStructNeighbors(particule);
#else
            List<particule_distance> neighbors = GetNeighbors(particule);
#endif

            //compute density and near-density
            foreach (particule_distance other in neighbors)
            {
                if (particule < other.j)
                {
                    float u = Vector2.Dot(particules_velocity[particule] - particules_velocity[other.j], other.vector);

                    if (u > 0)
                    {
                        Vector2 I = deltaT * (1 - other.q) * (sigma + beta * u) * u * other.vector / 2;
                        particules_velocity[particule] -= I / mass;
                        particules_velocity[particule] += I / mass;
                    }
                }
            }
        }
    }

    private void SavePreviousPosition()
    {
        for (int i = 0; i < n_particles; i++)
        {
            particules_previous_position[i] = particules_position[i];
        }
    }

    private void ApplyVelocity()
    {
        for (int i = 0; i < n_particles; i++)
        {
            particules_position[i] +=  particules_velocity[i] * deltaT;
        }
    }


    private void ApplySpringDeplacement()
    {
        float coef = alpha / mass;
        for (int i = 0; i < n_particles; i++)
        {
            if (particules_position[i].y < minY)
            {
                //particules_acceleration[i] += coef * (minY - particules_position[i].y) * Vector2.up;
                particules_position[i] += deltaT2 * coef * (minY - particules_position[i].y) * Vector2.up;
            }

            if (particules_position[i].x < minX)
            {
                //particules_acceleration[i] += coef * (minX - particules_position[i].x) * Vector2.right;
                particules_position[i] += deltaT2 * coef * (minX - particules_position[i].x) * Vector2.right;
            }

            if (particules_position[i].x > maxX)
            {
                //particules_acceleration[i] += coef * (particules_position[i].x - maxX) * Vector2.left;
                particules_position[i] += deltaT2 * coef * (particules_position[i].x - maxX) * Vector2.left;
            }
        }
    }

    private void DoubleDensityRelaxation()
    {

#if STRUCT || HASH
        CalculateNeighborsStruct();
#endif

        for (int particule = 0; particule < n_particles; particule++)
        {
            float rho = 0;
            float rho_near = 0;

#if STRUCT || HASH
            List<particule_distance> neighbors = GetStructNeighbors(particule);
#if TEST
            List<particule_distance> neighbors_tests = GetNeighbors(particule);
            foreach (particule_distance p in neighbors_tests)
            {
                if (!neighbors.Contains(p))
                {
                    Debug.LogError("MISSING NEIGHBOR : " + p.j);
                }
            }
#endif
#else
            List<particule_distance> neighbors = GetNeighbors(particule);
#endif

            //compute density and near-density
            foreach (particule_distance other in neighbors)
            {
                rho += Mathf.Pow(1 - other.q, 2);
                rho_near += Mathf.Pow(1 - other.q, 3);
            }

            //compute pressure and near-pressure
            float P         = k * (rho - rho_0);
            float P_near    = k_near * rho_near;
            Vector2 dx = Vector2.zero;

            foreach (particule_distance other in neighbors)
            {
                Vector2 D = deltaT2 * other.vector * (P * (1 - other.q) + P_near * Mathf.Pow(1 - other.q, 2));
                particules_position[other.j] += D / (2 * mass);
                dx -= D / (2 * mass);
            }

            particules_position[particule] += dx;
        }
    }

    private void RecalculateVelocity()
    {
        for (int i = 0; i < n_particles; i++)
        {
            particules_velocity[i] = (particules_position[i] - particules_previous_position[i]) / deltaT;
        }
    }

#if STRUCT
    private void InitNeighborsStruct()
    {
        for (int i = 0; i < neighbors_struct_nX * neighbors_struct_nY; ++i)
        {
            neighbors_struct[i][0] = 0;
        }
    }

    private void CalculateNeighborsStruct()
    {
        InitNeighborsStruct();

        for (int particule_index = 0; particule_index < n_particles; particule_index++)
        {
            int? struct_index = GetStructIndex(particule_index);
            if (struct_index != null)
            {
                int n = neighbors_struct[struct_index.GetValueOrDefault()][0] + 1;
                neighbors_struct[struct_index.GetValueOrDefault()][0] = n;
                neighbors_struct[struct_index.GetValueOrDefault()][n] = particule_index;
            }
        }

#if TEST
        int sum = 0;
        for (int i = 0; i < neighbors_struct_nX * neighbors_struct_nY; ++i)
            sum += neighbors_struct[i][0];

        if (sum != n_particles)
            Debug.LogError("NOT THE RIGHT AMOUNT IN STRUCT");
#endif
    }
#endif

#if HASH
    private void CalculateNeighborsStruct()
    {
        hash_grid = new Dictionary<Vector2Int, List<int>>();

        for (int particule_index = 0; particule_index < n_particles; particule_index++)
        {
            Vector2Int grid_index = GetStructXY(particule_index);

            List<int> hash_elements; 
            
            if(!hash_grid.TryGetValue(grid_index, out hash_elements))
            {
                hash_elements = new List<int>();
            }

            hash_elements.Add(particule_index);

            hash_grid[grid_index] = hash_elements;
        }
    }
#endif

#if HASH || STRUCT
    private Vector2Int GetStructXY(int particule_index)
{
    Vector2Int coordinates = new Vector2Int(
        Mathf.FloorToInt((particules_position[particule_index].x - minX) / neighbors_struct_size) + offset,
        Mathf.FloorToInt((particules_position[particule_index].y - minY) / neighbors_struct_size) + offset
    );
    return coordinates;
}
#endif

#if STRUCT
    private List<particule_distance> GetStructNeighbors(int particule_index)
    {
        Profiler.BeginSample("GetStructNeighbors");
        List<particule_distance> neighbors = new List<particule_distance>();

        Vector2Int coordinates = GetStructXY(particule_index);
        int? struct_index = GetStructIndex(coordinates);

        if(struct_index != null)
        {
            Vector2Int[] coordinates_list = new Vector2Int[9];
            coordinates_list[0] = coordinates;
            coordinates_list[1] = coordinates + Vector2Int.up + Vector2Int.left;
            coordinates_list[2] = coordinates + Vector2Int.up;
            coordinates_list[3] = coordinates + Vector2Int.up + Vector2Int.right;
            coordinates_list[4] = coordinates + Vector2Int.left;
            coordinates_list[5] = coordinates + Vector2Int.right;
            coordinates_list[6] = coordinates + Vector2Int.down + Vector2Int.left;
            coordinates_list[7] = coordinates + Vector2Int.down;
            coordinates_list[8] = coordinates + Vector2Int.down + Vector2Int.right;

            foreach(Vector2Int coord in coordinates_list)
            {
                struct_index = GetStructIndex(coord);


                if (struct_index != null)
                {
                    for(int i = 1; i <= neighbors_struct[struct_index.GetValueOrDefault()][0]; ++i)
                    {
                        particule_distance p = new particule_distance();
                        p.j = neighbors_struct[struct_index.GetValueOrDefault()][i];
                        p.vector = particules_position[p.j] - particules_position[particule_index];
                        p.q = p.vector.magnitude / h;

                        if(p.q < 1)
                        {
                            p.vector.Normalize();
                            neighbors.Add(p);
                        }
                    }
                }
            }
        }

        Profiler.EndSample();
        return neighbors;
    }

    private int? GetStructIndex(int particule_index)
    {
        Vector2Int coordinates = GetStructXY(particule_index);
        return GetStructIndex(coordinates);
    }

    private int? GetStructIndex(Vector2Int coordinates)
    {
        bool inStruct = (coordinates.x >= 0) && (coordinates.x < neighbors_struct_nX) && (coordinates.y >= 0) && (coordinates.y < neighbors_struct_nY);
        if (inStruct)
        {
            return coordinates.x + coordinates.y * neighbors_struct_nX;
        }
        else
        {
            return null;
        }
    }
#endif

#if HASH
    private List<particule_distance> GetStructNeighbors(int particule_index)
    {


        List<particule_distance> neighbors = new List<particule_distance>();

        Vector2Int coordinates = GetStructXY(particule_index);

        Vector2Int[] coordinates_list = new Vector2Int[9];
        coordinates_list[0] = coordinates;
        coordinates_list[1] = coordinates + Vector2Int.up + Vector2Int.left;
        coordinates_list[2] = coordinates + Vector2Int.up;
        coordinates_list[3] = coordinates + Vector2Int.up + Vector2Int.right;
        coordinates_list[4] = coordinates + Vector2Int.left;
        coordinates_list[5] = coordinates + Vector2Int.right;
        coordinates_list[6] = coordinates + Vector2Int.down + Vector2Int.left;
        coordinates_list[7] = coordinates + Vector2Int.down;
        coordinates_list[8] = coordinates + Vector2Int.down + Vector2Int.right;

        foreach (Vector2Int grid_index in coordinates_list)
        {

            List<int> hash_elements;

            if (hash_grid.TryGetValue(grid_index, out hash_elements))
            {
                foreach(int other in hash_elements)
                {
                    particule_distance p = new particule_distance();
                    p.j = other;
                    p.vector = particules_position[other] - particules_position[particule_index];
                    p.q = p.vector.magnitude / h;
                    if(p.q < 1)
                    {
                        p.vector.Normalize();
                        neighbors.Add(p);
                    }
                }
            }
        }

        return neighbors;
    }
#endif

    struct particule_distance
    {
        public int j;
        public Vector2 vector;
        public float q;
    }

    private List<particule_distance> GetNeighbors(int particule)
    {
        List<particule_distance> neighbors = new List<particule_distance>();

        for (int i = 0; i < n_particles; i++)
        {
            if (i != particule)
            {
                Vector2 vector = particules_position[i] - particules_position[particule];

                if (vector.magnitude < h)
                {
                    particule_distance p = new particule_distance();
                    p.j = i;
                    p.vector = vector;
                    p.q = vector.magnitude / h;

                    p.vector.Normalize();
                    neighbors.Add(p);
                }
            }
        }

        return neighbors;
    }
}
